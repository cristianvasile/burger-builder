import React from "react";
import { createUseStyles } from "react-jss";
import cc from "classcat";

const useStyles = createUseStyles({
  button: {
    backgroundColor: "transparent",
    border: "none",
    color: "white",
    outline: "none",
    cursor: "pointer",
    font: "inherit",
    padding: "10px",
    margin: "10px",
    fontWeight: "bold",

    "&:first-of-type": {
      marginLeft: 0,
      paddingLeft: 0,
    },
  },

  success: {
    color: "#5C9210",
  },

  danger: {
    color: "#944317",
  },
});

const Button = ({ btnType, children, clicked }) => {
  const styles = useStyles();
  return (
    <button
      className={cc([
        styles.button,
        {
          [styles.success]: btnType === "success",
          [styles.danger]: btnType === "danger",
        },
      ])}
      onClick={clicked}
    >
      {children}
    </button>
  );
};

export default Button;
