import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  "@keyframes load3": {
    "0%": {
      transform: "rotate(0deg)",
    },
    "100%": {
      transform: "rotate(360deg)",
    },
  },
  loader: {
    fontSize: "10px",
    margin: "50px auto",
    textIndent: "-9999em",
    width: "11em",
    height: "11em",
    borderRadius: "50%",
    background:
      "linear-gradient(to right, #492c6d 10%, rgba(73,44,109, 0) 42%)",
    position: "relative",
    animation: "$load3 1.4s infinite linear",
    transform: "translateZ(0)",

    "&::before": {
      width: "50%",
      height: "50%",
      background: "#fff",
      borderRadius: "100% 0 0 0",
      position: "absolute",
      top: "0",
      left: "0",
      content: '""',
    },

    "&::after": {
      background: "#fff",
      width: "75%",
      height: "75%",
      borderRadius: "50%",
      content: '""',
      margin: "auto",
      position: "absolute",
      top: "0",
      left: "0",
      bottom: "0",
      right: "0",
    },
  },
});
const Spinner = () => {
  const styles = useStyles();
  return <div className={styles.loader}>Loading...</div>;
};

export default Spinner;
