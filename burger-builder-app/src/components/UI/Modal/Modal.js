import React from "react";
import { createUseStyles } from "react-jss";
import Aux from "../../../hoc/Aux/Aux";
import Backdrop from "../Backdrop/Backdrop";

const useStyles = createUseStyles({
  Modal: {
    position: "fixed",
    zIndex: "500",
    backgroundColor: "white",
    width: "70%",
    border: "1px solid #ccc",
    boxShadow: "1px 1px 1px black",
    padding: "16px",
    left: "15%",
    top: "30%",
    boxSizing: "border-box",
    transition: "all 0.3s ease-out",
  },

  "@media (min-width: 600px)": {
    Modal: {
      width: "500px",
      left: "calc(50% - 250px)",
    },
  },
});

const Modal = (props) => {
  const classes = useStyles();

  return (
    <Aux>
      <Backdrop show={props.show} clicked={props.modalClosed}/>
      <div
        className={classes.Modal}
        style={{
          transform: props.show ? "translateY(0)" : "translateY(-100vh)",
          opacity: props.show ? "1" : "0",
        }}
      >
        {props.children}
      </div>
    </Aux>
  );
};

export default Modal;
