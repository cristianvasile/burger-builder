import React from "react";
import { createUseStyles } from "react-jss";
import BurgerIngredient from "./BurgerIngredient/BurgerIngredient";

const useStyles = createUseStyles({
  Burger: {
    width: "100%",
    margin: "auto",
    height: 250,
    overflow: "scroll",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: "1.2rem",
    marginTop: "15%",
  },
  "@media(min-width:1000px)  and (min-height:700px)": {
    Burger: {
      width: "700px",
      height: "600px",
    },
  },

  "@media(min-width:500px)  and (min-height:401px)": {
    Burger: {
      width: "450px",
      height: "400px",
    },

    "@media(min-width:500px)  and (min-height:400px)": {
      Burger: {
        width: "350px",
        height: "300px",
      },
    },
  },
});

const Burger = (props) => {
  let transformedIngredients = Object.keys(props.ingredients)
    .map((igKey) => {
      return [...Array(props.ingredients[igKey])].map((_, i) => {
        return <BurgerIngredient key={igKey + i} type={igKey} />;
      });
    })
    .reduce((arr, el) => {
      return arr.concat(el);
    }, []);

  if (transformedIngredients.length === 0) {
    transformedIngredients = <p>Please start adding ingredients</p>;
  }

  const classes = useStyles();

  return (
    <div className={classes.Burger}>
      <BurgerIngredient type="bread-top" />
      {transformedIngredients}
      <BurgerIngredient type="bread-bottom" />
    </div>
  );
};

export default Burger;
