import React from "react";
import { createUseStyles } from "react-jss";
import BuildControl from "./BuildControl/BuildControl";

const controls = [
  { label: "Salad", type: "salad" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
  { label: "Meat", type: "meat" },
];

const useStyles = createUseStyles({
  BuildControls: {
    width: "100%",
    backgroundColor: "#CF8F2E",
    display: "flex",
    flexFlow: "column",
    alignItems: "center",
    boxShadow: "0 2px 1px #ccc",
    margin: "auto",
    padding: "10px 0",
  },

  "@keyframes enable": {
    "0%": {
      transform: "scale(1)",
    },
    "60%": {
      transform: "scale(1.1)",
    },
    "100%": {
      transform: "scale(1)",
    },
  },

  OrderButton: {
    backgroundColor: "#DAD735",
    outline: "none",
    cursor: "pointer",
    border: "1px solid #966909",
    color: "#966909",
    fontFamily: "inherit",
    fontSize: "1.2em",
    padding: "15px 30px",
    boxShadow: "2px 2px 2px #966909",

    "&:hover, &:active": {
      backgroundColor: "#A0DB41",
      border: "1px solid #966909",
      color: "#966909",
    },

    "&:disabled": {
      backgroundColor: "#C7C6C6",
      cursor: "not-allowed",
      border: "1px solid #ccc",
      color: "#888888",
    },

    "&:not(:disabled)": {
      animation: "$enable 0.3s linear",
    },
  },
});
const BuildControls = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.BuildControls}>
      <p>
        Current Price: <strong>${props.price.toFixed(2)}</strong>
      </p>
      {controls.map((ctrl) => (
        <BuildControl
          key={ctrl.label}
          label={ctrl.label}
          added={() => props.ingredientAdded(ctrl.type)}
          removed={() => props.ingredientRemoved(ctrl.type)}
          disabled={props.disabled[ctrl.type]}
        />
      ))}

      <button
        className={classes.OrderButton}
        disabled={!props.purchasable}
        onClick={props.ordered}
      >
        ORDER NOW
      </button>
    </div>
  );
};

export default BuildControls;
