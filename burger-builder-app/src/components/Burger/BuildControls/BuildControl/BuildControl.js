import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  BuildControl: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "5px 0",
  },

  Label: {
    padding: "10px",
    fontWeight: "bold",
    width: "80px",
  },

  BuildControlButtonLess: {
    display: "block",
    font: "inherit",
    padding: "5px",
    margin: "0 5px",
    width: "80px",
    border: "1px solid #AA6817",
    cursor: "pointer",
    outline: "none",

    "&:disabled": {
      backgroundColor: "#AC9980",
      border: "1px solid #7E7365",
      color: "#ccc",
      cursor: "default",
    },

    "&:hover:disabled": {
      backgroundColor: "#AC9980",
      color: "#ccc",
      cursor: "not-allowed",
    },
    backgroundColor: "#D39952",
    color: "white",
    "&:hover, &:active": {
      backgroundColor: "#DAA972",
      color: "white",
    },
  },

  BuildControlButtonMore: {
    display: "block",
    font: "inherit",
    padding: "5px",
    margin: "0 5px",
    width: "80px",
    border: "1px solid #AA6817",
    cursor: "pointer",
    outline: "none",

    "&:disabled": {
      backgroundColor: "#AC9980",
      border: "1px solid #7E7365",
      color: "#ccc",
      cursor: "default",
    },

    "&:hover:disabled": {
      backgroundColor: "#AC9980",
      color: "#ccc",
      cursor: "not-allowed",
    },
    backgroundColor: "#8F5E1E",
    color: "white",

    "&:hover, &:active": {
      backgroundColor: "#99703F",
      color: "white",
    },
  },
});

const BuildControl = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.BuildControl}>
      <div className={classes.Label}>{props.label}</div>
      <button
        className={classes.BuildControlButtonLess}
        onClick={props.removed}
        disabled={props.disabled}
      >
        Less
      </button>
      <button className={classes.BuildControlButtonMore} onClick={props.added}>
        More
      </button>
    </div>
  );
};

export default BuildControl;
