import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  navigationItem: {
    margin: "10px 0",
    boxSizing: "border-box",
    display: "block",
    width: "100%",
    alignItems: "center",
  },

  navigationItemText: {
    color: "#8F5C2c",
    textDecoration: "none",
    width: "100%",
    boxSizing: "border-box",
    display: "block",
    padding: "0px 10px ",


    "&:hover": {
      backgroundColor: "#8f5c2c",
      borderBottom: "4px solid #40A4C8",
      color: "white",
    },

    "&:active": {
      backgroundColor: "#8f5c2c",
      borderBottom: "4px solid #40A4C8",
      color: "white",
    },
  },

  navigationTextActive: {
    color: "#40A4C8",
    textDecoration: "none",
    height: "100%",
    padding: "16px 10px ",
  },

  "@media (min-width: 500px)": {
    navigationItem: {
      margin: "0",
      display: "flex",
      height: "100%",
      width: "auto",
      alignItems: "center",
    },

    navigationItemText: {
      color: "white",
      height: "100%",
      padding: "16px 10px ",
      borderBottom: "4px solid transparent",

      "&:hover": {
        backgroundColor: "#8f5c2c",
        borderBottom: "4px solid #40A4C8",
        color: "white",
      },

      "&:active": {
        backgroundColor: "#8f5c2c",
        borderBottom: "4px solid #40A4C8",
        color: "white",
      },
    },

    navigationTextActive: {
      backgroundColor: "#703B09",
      borderBottom: "4px solid #40A4C8",
      color: "white",
      textDecoration: "none",
      height: "100%",
      padding: "16px 10px ",
    },
  },
});

const NavigationItem = (props) => {
  const styles = useStyles();

  return (
    <li className={styles.navigationItem}>
      <a
        href={props.link}
        className={
          props.active ? styles.navigationTextActive : styles.navigationItemText
        }
      >
        {props.children}
      </a>
    </li>
  );
};

export default NavigationItem;
