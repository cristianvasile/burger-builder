import React from "react";
import { createUseStyles } from "react-jss";
import NavigationItem from "./NavigationItem/NavigationItem";

const useStyles = createUseStyles({
  navigationItems: {
    margin: "0",
    padding: "0",
    listStyle: "none",
    display: "flex",
    flexFlow:'column',
    alignItems: "center",
    height: "100%",
  },

  '@media(min-width:500px)': {
    navigationItems:{
      flexFlow:'row'
    }
  }
});
const NavigationItems = (props) => {
  const styles = useStyles();

  return (
    <ul className={styles.navigationItems}>
      <NavigationItem link="/" active={true}>
        Burger Builder
      </NavigationItem>
      <NavigationItem link="/">Checkout</NavigationItem>
    </ul>
  );
};

export default NavigationItems;
