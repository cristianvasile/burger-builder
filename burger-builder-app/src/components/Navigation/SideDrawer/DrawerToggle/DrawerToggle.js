import React from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  drawerToggle: {
    width: "40px",
    height: "100%",
    display: "flex",
    flexFlow: "column",
    justifyContent: "space-around",
    alignItems: "center",
    padding: "10px 0",
    boxSizing: "border-box",
    cursor: "pointer",
  },

  drawerToggleDiv: {
    width: "90%",
    height: "3px",
    backgroundColor: "white",
  },

  "@media (min-width:500px)": {
    drawerToggle: {
      display: "none",
    },
  },
});

const DrawerToggle = (props) => {
  const styles = useStyles();
  return (
    <div onClick={props.clicked} className={styles.drawerToggle}>
      <div className={styles.drawerToggleDiv}></div>
      <div className={styles.drawerToggleDiv}></div>
      <div className={styles.drawerToggleDiv}></div>
    </div>
  );
};

export default DrawerToggle;
