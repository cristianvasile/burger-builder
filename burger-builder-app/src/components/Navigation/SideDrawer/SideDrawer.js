import React from "react";
import { createUseStyles } from "react-jss";
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import Backdrop from "../../UI/Backdrop/Backdrop";
import Aux from "../../../hoc/Aux/Aux";
import cc from "classcat";

const useStyles = createUseStyles({
  sideDrawer: {
    position: "fixed",
    width: "280px",
    maxWidth: "70%",
    height: "100%",
    left: 0,
    top: 0,
    zIndex: 200,
    backgroundColor: "white",
    padding: "32px 16px",
    boxSizing: "border-box",
    transition: "transform 0.3s ease-out",
  },

  "@media (min-width: 500px)": {
    sideDrawer: {
      display: "none",
    },
  },

  open: { transform: "translateX(0)" },

  close: { transform: "translateX(-100%)" },

  logo: {
    height: "11%",
    marginBottom: "32px",
  },
});

const SideDrawer = (props) => {
  const styles = useStyles();
  let attachedStyles = [styles.sideDrawer, styles.close];

  if (props.open) {
    attachedStyles = [styles.sideDrawer, styles.open];
  }

  return (
    <Aux>
      <Backdrop show={props.open} clicked={props.closed} />
      <div className={attachedStyles.join(" ")}>
        <div className={styles.logo}>
          <Logo />
        </div>

        <nav>
          <NavigationItems />
        </nav>
      </div>
    </Aux>
  );
};

export default SideDrawer;
