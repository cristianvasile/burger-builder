import React from "react";
import { createUseStyles } from "react-jss";
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import DrawerToggle from "../SideDrawer/DrawerToggle/DrawerToggle";

const useStyles = createUseStyles({
  toolbar: {
    height: "56px",
    width: "100%",
    position: "fixed",
    top: 0,
    left: 0,
    backgroundColor: "#703B09",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "0 20px",
    boxSizing: "border-box",
    zIndex: 90,
  },

  desktopOnly: {
    "@media (max-width: 499px)": {
      display: "none",
    },
  },

  logo: {
    height: "80%",
  },
});

const Toolbar = (props) => {
  const styles = useStyles();

  return (
    <header className={styles.toolbar}>
      <DrawerToggle clicked={props.drawerToggleClicked} />
      <div className={styles.logo}>
        <Logo />
      </div>

      <nav className={styles.desktopOnly}>
        <NavigationItems />
      </nav>
    </header>
  );
};

export default Toolbar;
