import React from "react";
import burgerLogo from "../../assets/Images/burger-logo.png";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  background: {
    backgroundColor: "white",
    padding: "8px;",
    height: "100%",
    boxSizing: "border-box",
    borderRadius: "5px",
  },

  logo: {
    height: "100%",
  },
});

const Logo = (props) => {
  const styles = useStyles();

  return (
    <div className={styles.background}>
      <img src={burgerLogo} alt="My Burger" className={styles.logo} />
    </div>
  );
};

export default Logo;
